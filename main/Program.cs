﻿using GuthySVGLoader;
using System.Drawing;
using Svg;

namespace main
{

    class MainClass
	{
		public static void Main(string[] args)
		{
			Svg.SvgDocument doc = Svg.SvgDocument.Open("tests/circle.svg");
			
            ISvgRenderer svgRenderer = new GuthySVGLoader.CambamRenderer();

			doc.RenderElement(svgRenderer);
			System.Console.ReadKey();
		}
	}
	//-------------------------------------------------------------------
}
