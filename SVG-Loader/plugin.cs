﻿/* ---------------------------------------------------------------------
 * Entry Point MAIN
 * Plugin Initialisazion.
 * ---------------------------------------------------------------------
 */
using System;
using System.Reflection;
using CamBam.UI;
using CamBam;
using Svg;

namespace GuthySVGLoader
{
	
	public class Handler : CamBam.CAD.CADFileIO
	{
		public override string FileFilter {
			get {
				return "SVG File (*.svg)|*.svg";
			}
		}

		public override bool ReadFile (string path)
		{
			Svg.SvgDocument doc = Svg.SvgDocument.Open(path);

			ISvgRenderer svgRenderer = new CambamRenderer();

			doc.RenderElement(svgRenderer);

			return true;
		}
	}

	public class Plugin
	{
		public static CamBamUI _ui;

		public static void InitPlugin (CamBamUI ui)
		{
			CamBamUI.CADFileHandlers.Add (new Handler ());
		}
		static string version() {
			Assembly assem = typeof(GuthySVGLoader.Plugin).Assembly;
			string version = assem.GetName().Version.ToString();
			return version;
		}
	}
}