﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using CamBam.Geom;
using CamBam.UI;
using Svg;

namespace GuthySVGLoader
{
    public class CambamRenderer : ISvgRenderer
    {
        public CambamRenderer()
        {
            this.SetBoundable(new GenericBoundable(new System.Drawing.RectangleF(0, 0, 1000, 1000)));
        }

        private readonly Stack<ISvgBoundable> _boundables = new Stack<ISvgBoundable>();

        private Region _clip = new Region();
        private Matrix _transform = new Matrix();

        public void SetBoundable(ISvgBoundable boundable)
        {
            _boundables.Push(boundable);
        }
        public ISvgBoundable GetBoundable()
        {
            return _boundables.Peek();
        }
        public ISvgBoundable PopBoundable()
        {
            return _boundables.Pop();
        }

        public float DpiY
        {
            get { return 96; }
        }

        public void DrawImage(Image image, RectangleF destRect, RectangleF srcRect, GraphicsUnit graphicsUnit)
        {
        }
        public void DrawImage(Image image, RectangleF destRect, RectangleF srcRect, GraphicsUnit graphicsUnit, float opacity)
        {
        }

        public void DrawImageUnscaled(Image image, Point location)
        {
        }
        private void draw(CamBam.CAD.Entity entity)
        {
            if(CamBam.UI.CamBamUI.MainUI != null)
            {
                CamBamUI.MainUI.ActiveView.CADFile.Add(entity);
            } else
            {
                Console.WriteLine("No CamBam instance found");
                Console.WriteLine("Adding entity {0}", entity);
            }
            
        }
        public void DrawPath(Pen pen, GraphicsPath path)
        {
            var newPath = (GraphicsPath)path.Clone();
            newPath.Transform(_transform);
            newPath.Flatten();

            // var np2 = (GraphicsPath)path.Clone();
            // np2.Transform(_transform);
            // 
            // for (int i = 0; i < np2.PathPoints.Length; i++)
            // {
            //     var x = np2.PathPoints[i].X;
            //     var y = np2.PathPoints[i].Y * -1;
            //     var pl = new CamBam.CAD.PointList(new CamBam.Geom.Point3F(x, y, 0));
            //     pl.Tag = np2.PathTypes[i].ToString();
            //     CamBamUI.MainUI.ActiveView.CADFile.Add(pl);
            // }

            Point3F _start = new Point3F();
            CamBam.CAD.Polyline polyline = new CamBam.CAD.Polyline(); ;
            for (int i=0; i < newPath.PathPoints.Length; i++) 
            {
                var x = newPath.PathPoints[i].X;
                var y = newPath.PathPoints[i].Y * -1;
                Point3F _current = new CamBam.Geom.Point3F(x, y, 0);

                switch (newPath.PathTypes[i])
                {
                    case 0:
                        if (polyline.NumSegments > 0)
                        {
                            draw(polyline);
                        }
                        _start = _current;
                        polyline = new CamBam.CAD.Polyline();
                        polyline.Add(_start);
                        break;
                    case 128:
                    case 129:
                    case 131:
                        polyline.Closed = true;
                        polyline.Add(_current);
                        polyline.Add(_start);
                        draw(polyline);

                        polyline = new CamBam.CAD.Polyline();
                        polyline.Add(_current);
                        break;
                    default:
                        polyline.Add(_current);
                        break;
                }
            }
            if(polyline.NumSegments > 0)
            {
                draw(polyline);
            }
        }
        public void FillPath(Brush brush, GraphicsPath path)
        {
            this.DrawPath(null, path);
        }
        public Region GetClip()
        {
            return _clip;
        }
        public void RotateTransform(float fAngle, MatrixOrder order = MatrixOrder.Append)
        {
            _transform.Rotate(fAngle, order);
        }
        public void ScaleTransform(float sx, float sy, MatrixOrder order = MatrixOrder.Append)
        {
            _transform.Scale(sx, sy, order);
        }
        public void SetClip(Region region, CombineMode combineMode = CombineMode.Replace)
        {
            return;
        }
        public void TranslateTransform(float dx, float dy, MatrixOrder order = MatrixOrder.Append)
        {
            _transform.Translate(dx, dy, order);
        }

        public SmoothingMode SmoothingMode
        {
            get { return SmoothingMode.Default; }
            set { /* Do Nothing */ }
        }

        public Matrix Transform
        {
            get { return _transform?.Clone(); }
            set
            {
                if (_transform != null)
                    _transform.Dispose();
                _transform = value?.Clone();
            }
        }

        public void Dispose()
        {
            if (_clip != null)
                _clip.Dispose();
            if (_transform != null)
                _transform.Dispose();
        }
    }
}
