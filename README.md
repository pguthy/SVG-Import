# SVG-Import

SVG-Import is a plugin for the CAD/CAM software CamBam. It add a file handler for SVG files. (Drag/drop or File/Open).

Downlod binary 
[SVG-Loader.dll](https://gitlab.com/maot0341/SVG-Import/-/raw/master/SVG-Loader/bin/Debug/SVG-Loader.dll)

Setup: drop the DLL-File into the plugin folder & (re-)start program.

Supported features:


* rect, circle, line, path
* transformations (scale, translate, viewbox, composites)
* units (metric only): mm, cm, dm, m
* ~~groups (mapped to CamBam layers)~~
* tag-id (mapped to CamBam object notes)

Limitations:

* path: arc with aspect ratio=1 only. (circle, not ellipse)
* ~~path: bezier continuations [t,T] not (yet) supported.~~

The projecet:

* C# source code
* IDE MonoDevelop v5.10

Folders:

* SVG-Loader/ ... the plugin source code for DLL
* main/ ... test console program (stdout)
* data/ ... sample data
